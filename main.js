// test(10000);

//test graph generation functions
// console.log("Random Cost Matrix: 5x5, Max Cost: 9")
// generateRandomCostMatrix(5, 9)
// console.log(" ")
// console.log("Random Euclidean Cost Matrix: 5x5, Max X: 9, Max Y: 4")
// generateRandomEuclideanCostMatrix(5, 9, 4)
// console.log(" ")
// console.log("Random Circular Euclidean Cost Matrix, 6x6, Radius: 10")
// generateRandomCircularGraphCostMatrix(6, 10)
// console.log(" ")
// greedyTSP(generateRandomCircularGraphCostMatrix(6, 10));

//greedy TSP testing
// greedyTSP(generateRandomCircularGraphCostMatrix(6, 10));
// greedyTSP(generateRandomEuclideanCostMatrix(6, 9, 9));
// greedyTSP(generateRandomCostMatrix(6, 9, 9));

//brute TSP testing
// bruteTSP(generateRandomCircularGraphCostMatrix(6, 10))
// bruteTSP(generateRandomEuclideanCostMatrix(6, 9, 9))
// bruteTSP(generateRandomCostMatrix(5,9))


// largeTest(850)
// doubleTest(40)

function doubleTest(max) {
  var prev = timeTrial(2);
  console.log("#N    Time in Seconds");
  for(var N = 4; N < max; N++) {
    //get time
    var time = timeTrial(N);
    //time in seconds
    var s = time / 1000;
    //print the info
    console.log(`${N.toString().padStart(6, " ")} ${s}`);
    prev = time;
  }
}

function largeTest(max) {
    var prev = timeTrial(50);
    console.log("#N    Time in Seconds");
    console.log(`${"50".padStart(6, " ")} ${prev / 1000}`);
    for(var N = 100; N < max; N+=50) {
      //get time
      var time = timeTrial(N);
      //time in seconds
      var s = time / 1000;
      //print the info
      console.log(`${N.toString().padStart(6, " ")} ${s}`);
      prev = time;
    }
  }

  for(var i = 0; i < 850; i+= 50) {
    
      console.log(`${i.toString().padStart(6, " ")} ${Math.pow(i, 2)/ 1000} `)
  }
function timeTrial(max) {
  var times = []

//do the trial and do stopwatch things
  for(var i = 0; i < 2; i++) {
    var t1 = new Date();
    greedyTSP(generateRandomCircularGraphCostMatrix(max, 10))
    var t2 = new Date();
    if(t1 > t2) {
        t2.setDate(t2.getDate() + 1);
    }
    var diff = t2.getTime() - t1.getTime();
    times[i] = t2 - t1;
  }
  return times.reduce((acc, c) => acc + c, 0) / times.length;
}

function generateRandomCostMatrix(vertices, max) {
    //initialize matrix
    var matrix = [];
    for(var i = 0; i < vertices; i++) {
        matj = new Array();
        for(var j = 0; j < vertices; j++) {
            matj.push(Infinity)
        }
        matrix[i] = matj;
    }
    //create edges
    for(var i = 0; i < vertices; i++) {
        for(var j = 0; j < vertices; j++) {
            //if we're not on the diagonal
            if(i != j) {
                var cost = Math.floor(Math.random() * (max - 1 + 1) + 1);
                matrix[i][j] = cost;
                matrix[j][i] = matrix[i][j];
            }
        }

        //make sure each node has at least one edge
        //does this row have an edge?
        if(matrix[i].filter((x) => x != Infinity).length == 0) {
            //if not, then pick a random vertex in this row and make an edge
            var whichToMake = Math.floor(Math.random() * ((vertices - 1) - 0 + 1) + 0);
            var cost = Math.floor(Math.random() * (max - 0 + 1) + 0);
            matrix[i][whichToMake] = cost;
            matrix[whichToMake][i] = matrix[i][whichToMake];
        }
    }

    printMatrix(matrix)
    return matrix
}

function printMatrix(matrix) {
    for(var i = 0; i < matrix.length; i++) {
        console.log(matrix[i].toString())
    }
}

function generateRandomEuclideanCostMatrix(vertices, maxX, maxY) {
    //initialize matrix
    var matrix = [];
    var nodes = [];
    for(var i = 0; i < vertices; i++) {
        matj = new Array();
        for(var j = 0; j < vertices; j++) {
            matj.push(Infinity)
        }
        matrix[i] = matj;
    }

    //create vertices
    for(var i = 0; i < vertices; i++) {
        const x = Math.floor(Math.random() * (maxX - 0 + 1) + 0);
        const y = Math.floor(Math.random() * (maxY - 0 + 1) + 0);
        nodes.push({"v": i, "x": x, "y": y})
    }
    //create edges
    for(var i = 0; i < vertices; i++) {
        for(var j = 0; j < vertices; j++) {
            //if we should create edge and we're not on the diagonal, create it
            if(i != j) {
                var cost = getDistance(nodes[i], nodes[j]);
                matrix[i][j] = cost;
                matrix[j][i] = matrix[i][j];
            }
        }

        //make sure each node has at least one edge
        //does this row have an edge?
        if(matrix[i].filter((x) => x != Infinity).length == 0) {
            //if not, then pick a random vertex in this row and make an edge
            //if  we're not on the diagonal, create it
            if(i != j) {
                var cost = getDistance(nodes[i], nodes[j]);
                matrix[i][j] = cost;
                matrix[j][i] = matrix[i][j];
            }
        }
    }
    // console.log("Adjacency Matrix:")
    // printMatrix(matrix)
    // console.log("Vertices and Coordinates:")
    // console.log(nodes)
    return {matrix: matrix, nodes: nodes}
}

//pythagorean theorem
function getDistance(node1, node2) {
    return Math.round(Math.pow(Math.pow(node1.x - node2.x, 2) + Math.pow(node1.y - node2.y, 2), 0.5))
}

function generateRandomCircularGraphCostMatrix(vertices, radius) {
    //initialize matrix
    var matrix = [];
    var nodes = [];
    for(var i = 0; i < vertices; i++) {
        matj = new Array();
        for(var j = 0; j < vertices; j++) {
            matj.push(Infinity)
        }
        matrix[i] = matj;
    }
    //make a list of vertices
    var tmp = [];
    for(var i = 0; i < vertices; i++) {
        tmp.push(i);
    }
    //randomize the vertices
    for(var i = tmp.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = tmp[i];
        tmp[i] = tmp[j];
        tmp[j] = temp;
    }
    //create vertices
    var angle = (2 * 3.14)/vertices;
    for(var i = 0; i < vertices; i++) {
        const x = radius * Math.sin(i * angle);
        const y = radius * Math.cos(i * angle);
        nodes.push({"v": tmp[i], "x": Math.round(x), "y": Math.round(y)})
    }
    //create edges
    for(var i = 0; i < vertices; i++) {
        for(var j = 0; j < vertices; j++) {
            //if we should create edge and we're not on the diagonal, create it
            if(i != j) {
                var cost = getDistance(nodes[i], nodes[j]);
                matrix[i][j] = cost;
                matrix[j][i] = matrix[i][j];
            }
        }
    }
    // console.log("Adjacency Matrix:")
    // printMatrix(matrix)
    // console.log("Vertices and Coordinates:")
    // console.log(nodes)
    return {matrix: matrix, nodes: nodes}
}



//greedy
function greedyTSP(matrix) {
    //implement Prim's algorithm
    var path = [];
    var visited = [];
    var edges = [];
    const isEuclidean = matrix.hasOwnProperty("matrix")
    var nodes;
    var minEdge = [null, null, Infinity];

    if(isEuclidean) {
        nodes = matrix.nodes;
        matrix = matrix.matrix;
    }
    var vertex = 0;
    while(path.length !== matrix.length - 1) {
        if(isEuclidean) {
           visited.push(nodes.filter((n) => n.v === vertex)[0]);
        } else {
            visited.push(vertex)
        }
        //get potential edges for this vertex
        for(var i = 0; i < matrix.length; i++) {
            //if cost is not infinite
            if(matrix[vertex][i] != Infinity) {
                edges.push([vertex, i, matrix[vertex][i]])
            }
        }

        //find minimum edge for this vertex that has not been visited yet
        for(var i = 0; i < edges.length; i++) {
            var hasBeenVisited = false;
            if(isEuclidean) {
                hasBeenVisited = visited.filter((n) => n.v === edges[i][1]).length > 0
            } else {
                hasBeenVisited = visited.indexOf(edges[i][1]) != -1
            }
            //
            if(edges[i][2] <= minEdge[2] && !hasBeenVisited) {
                minEdge = edges[i]
            }
        }
        edges.splice(edges.indexOf(minEdge), 1);
        path.push(minEdge);
        
        vertex = minEdge[1];
        minEdge = [null, null, Infinity]
    }

    //calculate cost
    var cost = 0;
    for(var i = 0; i < path.length; i++) {
        cost += path[i][2]
    }

    //get the cost of the going home
    cost +=  matrix[path[path.length - 1][1]][path[0][0]]

    // console.log("Cost: " + cost);

    // printPath(path)
    return path;
}

function printPath(path) {
    var print = "";
    for(var i = 0; i < path.length; i++)  {
        if(i == 0) {
            print += `${path[i][0]} -> ${path[i][1]} -> `
        } else if(i == path.length - 1) {
            print += `${path[i][1]} -> ${path[(path.length - 1) - i][0]}`
        } else {
            print += `${path[i][1]} -> `
        }
    }
    // console.log(print)
}

//brute force TSP
function bruteTSP(matrix) {
    var isEuclidean = matrix.hasOwnProperty("matrix")
    var nodes = [];
    if(isEuclidean) {
        nodes = matrix.nodes;
        matrix = matrix.matrix;
        //sort the vertices now so that brute force is easier
        nodes = nodes.map((n) => n.v)
        nodes.sort((a, b) => a - b)
    } else {
        for(var i = 0; i < matrix.length; i++) {
            nodes.push(i)
        }
    }
    var combinations = permute(nodes)
    var shortestLength = Infinity;
    var shortestTraversal = [];
    //for all permutations, get the cost and set the shortest tour & cost
    for(var i = 0; i < combinations.length; i++) {
        var cost = getTraversalLength(combinations[i], matrix)
        if(cost < shortestLength) {
            shortestLength = cost;
            shortestTraversal = combinations[i]
        }
    }
    // console.log("Cost: " + shortestLength);
    //print traversal
    var print = ""
    for(var i = 0; i < shortestTraversal.length; i++) {
        if(i != shortestTraversal.length - 1) {
            print += `${shortestTraversal[i]} -> `
        } else {
            print += shortestTraversal[i] + " -> " + shortestTraversal[0]
        }
    }

    // console.log(print)
}


function getTraversalLength(route, matrix) {
    var cost = 0;
    //split the traversal from vertices into edges and add the return to home edge to the end
    var parts = []
    for(var i = 0; i < route.length; i++) {
        if(i == route.length - 1) {
            parts.push([route[i], route[0]])
        } else {
            parts.push([route[i], route[i + 1]])
        }
    }
    //get the cost of the tour
    for(var i = 0; i < route.length; i++) {
        cost += matrix[parts[i][0]][parts[i][1]]
    }

    return cost;
}

function dynamicTSP(matrix) {
    var isEuclidean = matrix.hasOwnProperty("matrix")
    if(isEuclidean) {
        nodes = matrix.nodes;
        matrix = matrix.matrix;
    }
    var table = [];
    var minCost = Infinity;
    
    //create table to represent all possible permutations
    for(var i = 0; i < (1<<matrix.length); i++) {
        table[i] = [];
        for(var j = 0; j < matrix.length; j++) {
            table[i][j] = -1;
        }
    }
    helper(1, 0)
    //recursive func
    function helper(bitmask, position) {
        //if visited
        if(table[bitmask][position] != -1 ) {
            return table[bitmask][position]
        }
    
        if(bitmask == (1<<matrix.length) -1) {
            return matrix[position][0];
        }
       
        //if unvisited
        for(var i = 0; i < matrix.length; i++) {
            if((bitmask &(1<<i)) == 0) {
                //get next cost
                var costs = matrix[position][i] + helper(bitmask|(1<<i), i);
                //find min cost
                minCost = Math.min(minCost, costs);
            }
        }
        //this returns the incorrect cost at the end of the whole recursion cycle
        // :/
        table[bitmask][position] = minCost
        return table[bitmask][position]
    }
    return minCost
}



//recursively create permutations
function permute(nodes) {
    var result = []
    const list = nodes
    if(list.length == 0) {
        result.push(list)
    }

    for(var i = 0; i < list.length; i++) {
        let x = permute(nodes.slice(0, i).concat(nodes.slice(i + 1)));
        if(x.length == 0) {
            result.push([nodes[i]])
        } else {
            for(var j = 0; j < x.length; j = j + 1) {
                result.push([nodes[i]].concat(x[j]));
            }
        }
    }
    return result
}